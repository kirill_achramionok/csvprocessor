package by.achramionok.dbentity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TableTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void generateInsertQueryWithNullFields() throws Exception {
        exception.expect(IllegalStateException.class);
        Table table = new Table();
        table.generateInsertQuery();
    }
}