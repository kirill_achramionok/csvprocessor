package by.achramionok;

import by.achramionok.dbentity.Table;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Stream;

public class Reader extends Thread {
    private static Logger log = LoggerFactory.getLogger(Reader.class.getName());

    private String path;
    private BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(40);
    private ThreadPoolExecutor executor = new ThreadPoolExecutor(
            20,
            20,
            0L,
            TimeUnit.MILLISECONDS,
            queue
    );
    private Table mapping;

    Reader(String path) {
        this.path = path;
        executor.setRejectedExecutionHandler((r, executor) -> {
            try {
                executor.getQueue().put(r);
            } catch (InterruptedException e) {
                log.error("Exception: " + e);
            }
        });
    }

    private void loadMapping(String mappingPath) {
        try {
            mapping = new ObjectMapper().readValue(new File(mappingPath),
                    Table.class);
        } catch (IOException ex) {
            log.error("Exception: " + ex);
        }
    }

    @Override
    public void run() {
        loadMapping("parameters.json");
        List<String> lines = new ArrayList<>();
        int[] counter = new int[]{0};
        long start = System.currentTimeMillis();
        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            stream.forEach(line -> {
                if (counter[0] % 1000 == 0) {
                    executor.execute(new Processor(new ArrayList<>(lines), mapping));
                    lines.clear();
                }
                lines.add(line);
                counter[0]++;
            });
        } catch (Exception ex) {
            log.error("Exception: " + ex);
        } finally {
            executor.shutdown();
        }
        long fin = System.currentTimeMillis();
        log.info("Finished with time: "+(fin - start) / 1000);
        System.out.println((fin - start) / 1000);
    }
}
