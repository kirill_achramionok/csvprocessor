package by.achramionok;

import by.achramionok.datasource.DataBaseHelper;
import by.achramionok.dbentity.Table;
import by.achramionok.dbentity.TableField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class Processor implements Runnable {

    private static Logger log = LoggerFactory.getLogger(Processor.class.getName());

    private List<String> strings;
    private Table mapping;

    public Processor(ArrayList<String> strings, Table mapping) {
        this.strings = strings;
        this.mapping = mapping;
    }

    @Override
    public void run() {
        String[] parametersList;
        try (Connection connection = DataBaseHelper.getConnection();
             PreparedStatement statement = connection.prepareStatement(mapping.generateInsertQuery())) {
            for (String parameters : strings) {
                parametersList = parameters.split(",");
                int i = 1;
                for (TableField field : mapping.getFields()) {
                    if (parametersList.length >= field.getIndex()) {
                        statement.setObject(i, field.convertToType(parametersList[field.getIndex() - 1]));
                    }
                    i++;
                }
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        } catch (Exception ex) {
            log.error("Exception: " + ex);
        }
    }

}
