package by.achramionok.dbentity;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TableField {

    private String name;
    private int index;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public <Type> Type convertToType(String parameter) throws Exception {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        if (type.equals("string")) {
            return (Type) parameter;
        } else if (type.equals("date")) {
            return (Type) new Date(format.parse(parameter).getTime());
        } else if (type.equals("long")) {
            return (Type) Long.valueOf(parameter);
        }
        return null;
    }
}
