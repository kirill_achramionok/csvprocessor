package by.achramionok.datasource;



import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DataBaseHelper {
    private static Logger log = LoggerFactory.getLogger(DataBaseHelper.class.getName());
    private static BasicDataSource dataSource;
    private static final String createQuery = "CREATE TABLE IF NOT EXISTS MyTable(" +
            "characters_set VARCHAR(50) NULL DEFAULT NULL ," +
            "numbers_set BIGINT NULL DEFAULT NULL, " +
            "random_date DATE NULL DEFAULT NULL" +
            ")";

    static {
        if (dataSource == null) {
            Properties properties = new Properties();
            try {
                FileInputStream inputStream = new FileInputStream("src\\main\\resources\\connection.properties");
                properties.load(inputStream);
                String driver = properties.getProperty("jdbc.driver");
                if (driver != null) {
                    Class.forName(driver);
                }
            }catch (Exception ex){
                System.out.println("Exception: " + ex);
            }
            BasicDataSource ds = new BasicDataSource();
            ds.setUrl(properties.getProperty("jdbc.url"));
            ds.setPassword(properties.getProperty("jdbc.password"));
            ds.setUsername(properties.getProperty("jdbc.username"));
            ds.setInitialSize(Integer.valueOf(properties.getProperty("jdbc.initialSize")));
            ds.setMinIdle(Integer.valueOf(properties.getProperty("jdbc.minIdle")));
            ds.setMaxIdle(Integer.valueOf(properties.getProperty("jdbc.maxIdle")));
            ds.setMaxTotal(Integer.valueOf(properties.getProperty("jdbc.maxTotal")));
            ds.setMaxOpenPreparedStatements(Integer.valueOf(properties.getProperty("jdbc.maxOpenPreparedStatement")));
            ds.setDefaultAutoCommit(Boolean.valueOf(properties.getProperty("jdbc.autoCommit")));
            dataSource = ds;
            log.info("Datasource initialized.");
        }
        try {
            initDatabase();
        } catch (SQLException e) {
            log.error("Exception: " + e);
        }
    }

    private static void initDatabase() throws SQLException {
        Connection connection = getConnection();
        Statement statement;
        try {
            statement = connection.createStatement();
            statement.execute(createQuery);
            statement.close();
        } finally {
            if (connection != null)
                connection.close();
        }
    }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
