package by.achramionok.dbentity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Table {
    private static Logger log = LoggerFactory.getLogger(Table.class.getName());

    private String name;
    private List<TableField> fields = new ArrayList<>();

    public String generateInsertQuery() {
        StringBuilder mainBuilder = new StringBuilder();
        StringBuilder valuesBuilder = new StringBuilder();
        valuesBuilder.append("VALUES(");
        mainBuilder.append("INSERT INTO ").append(name).append('(');
        if(fields.size() != 0) {
            for (TableField field : fields) {
                mainBuilder.append(field.getName()).append(",");
                valuesBuilder.append("?,");
            }
        }else{
            throw new IllegalStateException("0 parameters in mapping file!");
        }
        valuesBuilder.deleteCharAt(valuesBuilder.length() -1).append(')');
        mainBuilder.deleteCharAt(mainBuilder.length() - 1).append(") ");
        return mainBuilder.append(valuesBuilder.toString()).toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addField(TableField field){
        fields.add(field);
    }

    public void clearFields(){
        fields.clear();
    }

    public List<TableField> getFields() {
        return fields;
    }

    public void setFields(List<TableField> fields) {
        this.fields = fields;
    }
}
